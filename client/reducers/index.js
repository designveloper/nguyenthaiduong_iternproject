import { combineReducers } from 'redux'
import productsReducer from './productsReducer'
import uiReducer from './uiReducer'

export default combineReducers({
    products: productsReducer,
    ui: uiReducer
});