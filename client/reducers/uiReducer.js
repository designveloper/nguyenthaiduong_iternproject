import { TOGGLE_MODAL, TOGGLE_MODAL_AUTHEN } from '../../imports/ui/actions/types'

const initialState = {
    isModalActive: false,
    isModalLogin: false
};

export default function(state = initialState, actions) {
    switch(actions.type) {
        case TOGGLE_MODAL: 
            return {
                ...state,
                isModalActive: actions.payload
            }
        case TOGGLE_MODAL_AUTHEN: 
            return {
                ...state,
                isModalActive: actions.payload.activateModal,
                isModalLogin: actions.payload.isModalLogin
            }
        default:
            return {
                ...state
            }
    }
}