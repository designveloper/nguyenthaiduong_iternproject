import { FETCH_ALL_PRODUCTS, FETCH_A_PRODUCT } from '../../imports/ui/actions/types'

const initialState = {};

export default function(state = initialState, actions) {
    switch(actions.type) {
        case FETCH_ALL_PRODUCTS:
            return {
                ...state,
                products: actions.payload
            }
        case FETCH_A_PRODUCT:
            return {
                ...state,
                product: actions.payload
            }
        default:
            return state;
    }
}