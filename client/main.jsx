import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import App from '/imports/ui/App'
import store from './store'
import { renderRoutes } from '../imports/ui/router'

Meteor.startup(() => {
  render(
  <Provider store={store}>
    <App />
  </Provider>
  , document.getElementById('react-target'));
});
