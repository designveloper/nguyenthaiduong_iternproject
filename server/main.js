import { Meteor } from 'meteor/meteor';
import Links from '/imports/api/links';
import Products from '../imports/api/products'

function insertLink(title, url) {
  Links.insert({ title, url, createdAt: new Date() });
}

Meteor.startup(() => {
  // If the Links collection is empty, add some data.
  import '../imports/api/products/method';
  import '../imports/api/products/public';
  
  if(Products.find().count() === 2) {
    Meteor.call('addNewProduct', {
      name: 'new prod2',
      size: 10,
      color: "red",
      brand_id: "CL",
      price: 10000,
      available: true,
      category_id: "bikini"
    });
  }
  else {
    Meteor.call('editProduct', {
      product_id: 'xCRMmp2RqdLLJnwFu', 
      product: {
        name: 'new prod',
        size: 13,
        color: "red",
        brand_id: "zalora",
        price: 10000,
        available: true,
        category_id: "dresses"
      }
    })
  }
});
