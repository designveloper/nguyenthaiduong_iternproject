import React from 'react'

import './FooterForm.scss'

const FooterForm = props => {
    const { textFooter, nameLinkFooter } = props;

    return (
        <div className="footer--authenForm">
            <p className="text--footer">
                { textFooter } 
                <u className="link--footer">{ nameLinkFooter }</u>
            </p>
        </div>
    )
}

export default FooterForm