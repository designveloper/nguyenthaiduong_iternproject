import React from "react"

import InputAuthen from "../../Elements/Input/InputAuthen/InputAuthen"
import ButtonAuthen from "../../Elements/Button/ButtonAuthen/ButtonAuthen"
import FooterForm from "../FooterForm/FooterForm"
import "./Register.scss"

const Register = props => {

    return (
        <div className='register'>
            <h1 className="title--registerForm">Register</h1>
            <form className="form--register">
                <div className="inputGroup--registerForm">
                    <InputAuthen label="Name" type="text" placeholder="Enter your Name" error="This field required"/>
                    <InputAuthen label="Email" type="email" placeholder="Enter your Email" />
                    <InputAuthen label="Password " type="password" placeholder="Enter your Password" />
                </div>
                <div className="termPolicy--registerForm">
                    <p>
                        By creating an account you agree to the 
                    </p>
                    <p className="text--termPolicy">
                        <u className="textUnderline--termPolicy">Terms of Service </u> 
                        and 
                        <u className="textUnderline--termPolicy"> Privacy Policy</u>
                    </p>
                </div>
                <ButtonAuthen buttonName="Register" isValid={false}/>
            </form>
            <FooterForm textFooter="Do you have an account?" nameLinkFooter="Log In" />
        </div>
    )
}

export default Register 