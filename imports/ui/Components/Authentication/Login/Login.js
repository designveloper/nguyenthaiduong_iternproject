import React from "react"

import InputAuthen from "../../Elements/Input/InputAuthen/InputAuthen";
import ButtonAuthen from "../../Elements/Button/ButtonAuthen/ButtonAuthen";
import FooterForm from "../FooterForm/FooterForm";
import "./Login.scss";

const Login = (props) => (
        <div className="login">
            <h1 className="title--loginForm">Log In</h1>
            <form className="form--login">
                <div className="inputGroup--loginForm">
                    <InputAuthen label="Email" type="email" placeholder="Enter your Email" isInvalid={true}/>
                    <InputAuthen label="Password " type="password" placeholder="Enter your Password" />
                </div>
                <div className="rememberForgotPass--loginForm">
                    <div className="checkBox--loginForm">
                        <img src="/image/check-box-active.svg" />
                        <p className="text--checkBox">Remember password</p>
                    </div>
                    <p className="linkForgotPass--loginForm">Forgot your password?</p>
                </div>
                <ButtonAuthen buttonName="Log In" isValid={false}/>
            </form>
            <FooterForm textFooter="Don't have an account?" nameLinkFooter="Register" />
        </div>
    );

export default Login;
