import React from 'react'
import { Link } from 'react-router-dom'

import MenuItem from './MenuItem';

const MiniFooter = props => {
    const { menuItems } = props;

    return (
        <div className="mini-footer">
            <div className="mini-footer-menu">
                {
                    menuItems.map((menuItem, index) => {
                        return <MenuItem key={index} Url={menuItem.Url} linkName={menuItem.linkName} />
                    })
                }
            </div>
            <div className="policy-terms">
                <Link to="/">Privacy Policy</Link>
                <Link to="/">Terms & Conditions</Link>
            </div>
        </div>
    )
}

export default MiniFooter