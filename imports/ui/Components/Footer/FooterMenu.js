import React from 'react'

import MenuItem from './MenuItem'

const FooterMenu = props => {
    const { menuItems } = props;

    return (
        <div className='footer-menu'>
            {
                menuItems.map((menuItem, index) => {
                    return <MenuItem key={index} Url={menuItem.Url} linkName={menuItem.linkName} />
                })
            }
        </div>
    )
}

export default FooterMenu