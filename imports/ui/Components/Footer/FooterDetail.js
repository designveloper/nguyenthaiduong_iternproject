import React from 'react'
import { Link } from 'react-router-dom'

import FooterMenu from './FooterMenu'

const FooterDetail = props => {
    const { menuItems } = props;
    return (
        <>
            <div className="footer-detail">
                <Link to="/" className='footer-logo'>
                    <img src="/image/footer_logo.svg" />
                </Link>
                <FooterMenu menuItems={menuItems}/>
                <div className="footer-social">
                    <img src="/image/twitter.svg" />
                    <img src="/image/facebook.svg" />
                    <img src="/image/instagram.svg" />
                </div>
            </div>
            <div className="footer-line"></div>
        </>
    )
}

export default FooterDetail