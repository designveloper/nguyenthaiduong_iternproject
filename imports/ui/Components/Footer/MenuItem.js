import React from 'react'
import { Link } from 'react-router-dom'

const MenuItem = props => {
    return (
        <div className="menu-item">
            <Link to={props.Url}>{props.linkName}</Link>
        </div>
    )
}

export default MenuItem