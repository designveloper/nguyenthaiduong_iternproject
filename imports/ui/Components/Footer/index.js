import React from 'react'

import FooterDetail from './FooterDetail'
import MiniFooter from './MiniFooter'
import './index.scss'

const Footer = props => {
    let menuItems = [
        {linkName: 'Home', Url: '/'},
        {linkName: 'Products', Url: '/products'},
        {linkName: 'Services', Url: '/'},
        {linkName: 'About Us', Url: '/'},
        {linkName: 'Help', Url: '/'},
        {linkName: 'Contacts', Url: '/'},
    ];

    return (
        <div className="footer">
            <FooterDetail menuItems={menuItems} />
            <MiniFooter menuItems={menuItems} />
        </div>
    )
}

export default Footer