import React from 'react'

import './index.scss'

const DropDown = props => {
    const { name, children } = props;

    const handleDropDown = event => {
        console.log(event.currentTarget, event.currentTarget.querySelector('.invisible-content'));
        const invisibleContent = event.currentTarget.nextSibling;
        
        if(invisibleContent) {
            if(!invisibleContent.classList.contains('active')) {
                console.log('active');
                invisibleContent.classList.add('active');
            }
            else {
                console.log('deactive');
                invisibleContent.classList.remove('active');
            }
        }
    }

    return (
        <div className="dropdown">
            <div className='visible-button' onClick={handleDropDown}>
                <p className='button-name'>{name}</p>
                <img src='/image/arrow.svg' className='button-arrow-icon'/>
            </div>
            <div className='invisible-content'>
                {children}
            </div>
        </div>
    )
}

export default DropDown