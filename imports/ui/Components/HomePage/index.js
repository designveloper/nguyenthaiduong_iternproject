import React from 'react'

import './index.scss'

const HomePage = props => {
    return (
        <div className="home-page">
            <div className="banner">
                <div className="banner-image" style={{backgroundImage: `url(/image/banner.jpg)`}}>
                    <p className="text">outfit of the week</p>
                    <button>shop now</button>
                </div>
            </div>
            <div className="standout-prod">
                <div className="item">
                    <img src="https://via.placeholder.com/405" />
                    <div className="item-detail">
                        <p>Men</p>
                        <div className="line"></div>
                        <button>Shop now</button>
                    </div>
                </div>
                <div className="item">
                    <img src="https://via.placeholder.com/280" />
                    <div className="item-detail">
                        <p>Men</p>
                        <div className="line"></div>
                        <button>Shop now</button>
                    </div>
                </div>
                <div className="item">
                    <img src="https://via.placeholder.com/280" />
                    <div className="item-detail">
                        <p>Men</p>
                        <div className="line"></div>
                        <button>Shop now</button>
                    </div>
                </div>
                <div className="item">
                    <img src="https://via.placeholder.com/280" />
                    <div className="item-detail">
                        <p>Men</p>
                        <div className="line"></div>
                        <button>Shop now</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HomePage 