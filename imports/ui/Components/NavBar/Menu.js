import React from 'react'

import MenuItem from './MenuItem'

const Menu = props => {
    let menuItems = ['Men', 'Ladies', 'Girls', 'Boys'];

    return (
        <div className="menu">
            {
                menuItems.map((item, index) => {
                    return <MenuItem key={index} name={item} />
                })
            }
        </div>
    )
}

export default Menu