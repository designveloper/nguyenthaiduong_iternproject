import React from "react"

import { Link } from "react-router-dom"

const Header = props => {
    const { toggleAuthenModal } = props;

    return (
        <div className="header">
            <div className="search-input">
                <input type='text' placeholder="Search" />
                <img src="/image/search.svg" className="search-icon"/>
            </div>
            <div className="brand-name">
                <Link to="/"><img src="/image/logo.svg"/></Link>
            </div>
            <div className="header-button">
                <p className="Register" onClick={() => toggleAuthenModal(true, false)}>Register</p>
                <div className="Login" onClick={() => toggleAuthenModal(true, true)}>
                    <p>Login</p>
                </div>
                <div className="cart">
                    <img src="/image/cart.svg"/>
                    <div className="in-cart">
                        <p>0</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Header