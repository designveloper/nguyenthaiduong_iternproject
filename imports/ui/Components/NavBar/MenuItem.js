import React from 'react'

const MenuItem = props => {
    return (
        <div className="item">
            <p>{props.name}</p>
            <img src="/image/arrow.svg" />
        </div>
    )   
}

export default MenuItem