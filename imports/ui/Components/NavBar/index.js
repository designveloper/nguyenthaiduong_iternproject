import React from "react"

import Header from "./Header"
import Menu from "./Menu"
import Modal from "../Modal/Modal"
import Login from "../Authentication/Login/Login"
import Register from "../Authentication/Register/Register"
import "./index.scss"

const NavBar = props => {
    const { ui, toggleAuthenModal } = props;
    return (
        <>
            <div className="nav-bar">
                <Header toggleAuthenModal={toggleAuthenModal}/>
                <div className="line"></div>
                <Menu />
            </div>
            <Modal toggleModal={toggleAuthenModal} isOpen={ui.isModalActive}>
                {
                    ui.isModalLogin ? <Login /> : <Register />
                }
            </Modal>
        </>
    )
}

export default NavBar