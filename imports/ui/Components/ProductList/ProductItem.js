import React from 'react'

const ProductItem = props => {
    const { image, name, price } = props;
    
    return (
        <div className='product-item'>
            <img src={image} className="product-image"/>
            <div className='product-detail'>
                <p className="product-name">
                    {name}
                </p>
                <p className="product-price">{price}</p>
            </div>
        </div>
    )
}

export default ProductItem