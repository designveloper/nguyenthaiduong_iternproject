import React from 'react'
import Slider, { Range } from 'rc-slider'
import 'rc-slider/assets/index.css';

import CategoryButton from './CategoryButton'
import DropDown from '../Dropdown'
import Size from '../DropDownChild/Size/Size'
import PickColorRound from '../DropDownChild/PickColorRound/PickColorRound'
import CheckBox from '../DropDownChild/CheckBox/CheckBox'

const ControlPanel = props => {
    let categorySections = [
        {name: 'All dresses', status: true},
        {name: 'Rompers/Jumpsuit', status: false},
        {name: 'Casual dresses', status: false},
        {name: 'Going out dresses', status: false},
        {name: 'Party/Ocassion dresses', status: false},
        {name: 'Mini dresses', status: false},
        {name: 'Maxi/Midi dresses', status: false},
        {name: 'Sets', status: false}
    ]
    return (
        <div className='control-panel'>
            <h2 className='control-section-name'>Category</h2>
            {
                categorySections.map((button, index) => {
                    return <CategoryButton {...button} key={index} />
                })
            }
            <h2 className='control-section-name'>Filter</h2>
            <DropDown name="Size">
                <div className='content--dropdown-horizontal'>
                    <Size sizeName={'S'} state='active'/>
                    <Size sizeName={'M'} />
                    <Size sizeName={'L'} state='disabled'/>
                </div>
            </DropDown>
            <DropDown name="Color">
                <div className='content--dropdown-horizontal'>
                    <PickColorRound style={{backgroundColor: '#ff5f6d'}} />
                    <PickColorRound style={{backgroundColor: 'rgba(255, 213, 67, 0.4)'}} />
                    <PickColorRound style={{backgroundColor: 'rgba(95, 109, 255, 0.4)'}} />
                    <PickColorRound style={{backgroundColor: 'rgba(255, 161, 95, 0.4)'}} />
                </div>
            </DropDown>
            <DropDown name="Brand">
                <div className='content--dropdown-vertical'>
                    <CheckBox checkBoxValue='Zara' state='active' />
                    <CheckBox checkBoxValue="H&M" />
                </div>
            </DropDown>
            <DropDown name="Price">
                <div className="content--dropdown-horizontal">
                    <Range min={0} max={20} defaultValue={[3, 10]} tipFormatter={value => `${value}%`} style={{margin: '0 8px'}}/>
                </div>
            </DropDown>
            <DropDown name="Available">
                <div className="content--dropdown-vertical">
                    <CheckBox checkBoxValue="In-store" state='active' />
                    <CheckBox checkBoxValue="Out of stock" />
                </div>
            </DropDown>
            <div className='control-line'></div>
        </div>
    )
}

export default ControlPanel