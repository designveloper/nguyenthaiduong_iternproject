import React from 'react'

const CategoryButton = props => {
    const { name, status } = props;
    let categoryClasses = ['control-category'];
    if(status) {
        categoryClasses.push('active');
    }
    
    return (
        <p className={categoryClasses.join(' ')}>{name}</p>
    )
}

export default CategoryButton