import React from 'react'

import ProductItem from './ProductItem'
import ProductTitle from './ProductTitle'
import ControlPanel from './ControlPanel'
import './index.scss'

const ProductList = props => {
    let productItems = [
        {image: 'https://via.placeholder.com/180', name: 'Collete Stretch Linen Minidress', price: '$69.00'},
        {image: 'https://via.placeholder.com/180', name: 'Collete Stretch Linen Minidress', price: '$69.00'},
        {image: 'https://via.placeholder.com/180', name: 'Collete Stretch Linen Minidress', price: '$69.00'},
        {image: 'https://via.placeholder.com/180', name: 'Collete Stretch Linen Minidress', price: '$69.00'},
        {image: 'https://via.placeholder.com/180', name: 'Collete Stretch Linen Minidress', price: '$69.00'},
        {image: 'https://via.placeholder.com/180', name: 'Collete Stretch Linen Minidress', price: '$69.00'},
        {image: 'https://via.placeholder.com/180', name: 'Collete Stretch Linen Minidress', price: '$69.00'},
        {image: 'https://via.placeholder.com/180', name: 'Collete Stretch Linen Minidress', price: '$69.00'},
        {image: 'https://via.placeholder.com/180', name: 'Collete Stretch Linen Minidress', price: '$69.00'},
    ]

    return (
        <div className="product-list">
            <ProductTitle />
            <div className="product-container">
                <ControlPanel />
                <div className='product-table'>
                    {
                        productItems.map((product, index) => {
                            return <ProductItem {...product} key={index} />
                        })
                    }
                </div>
            </div>
        </div>
    )
}

export default ProductList