import React from 'react'

import './InputAuthen.scss'

const InputAuthen = props => {
    const { label, type, placeholder, error } = props;
    let inputClasses = ['input--authentication'];

    if(error !== undefined && error !== '') {
        inputClasses.push('isInvalid');
    }

    const showError = () => {
        if(!error || error != '') {
            return <p className="textError-authentication">{error}</p>
        } 
    }

    return (
        <div className="inputLabel--authentication">
            <label className="label--authentication">{label}</label>
            <input type={type} placeholder={placeholder} className={inputClasses.join(' ')}/>
            { showError() }
        </div>
    )
}

export default InputAuthen