import React from 'react'

import './ButtonAuthen.scss'

const ButtonAuthen = props => {
    const { buttonName, isValid } = props;
    let btnClasses = ['btnLogin--authenForm'];

    if(isValid) {
        btnClasses.push('valid');
    }
    else {
        btnClasses.push('invalid');
    }

    return (
        <button className={btnClasses.join(' ')}>{ buttonName }</button>
    )
}

export default ButtonAuthen