import React from 'react'

import './CheckBox.scss'

const CheckBox = props => {
    const { checkBoxValue, state } = props;

    let checkBoxClasses = ['checkbox-grey'];
    let checkBoxImage = 'check-box-unactive.svg';

    if(state === 'active') {
        checkBoxClasses.push('active');
        checkBoxImage = 'check-box-active.svg';
    }

    return (
        <div className={checkBoxClasses.join(' ')}>
            <img src={`/image/${checkBoxImage}`} />
            <p className="text--checkbox">{checkBoxValue}</p>
        </div>
    )
}

export default CheckBox