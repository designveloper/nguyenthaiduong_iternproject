import React from 'react'

import './PickColorRound.scss'

const PickColorRound = props => {
    const { style } = props;

    return (
        <div className="pickColorBtn-round" style={style}></div>
    )
}

export default PickColorRound
