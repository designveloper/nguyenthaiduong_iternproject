import React from 'react'

import './Size.scss'

const Size = props => {
    const { sizeName, state } = props;
    let smallSizeClasses = ['squareBtn-size'];

    if(state === 'active') {
        smallSizeClasses.push('active');
    }
    else if(state === 'disabled'){
        smallSizeClasses.push('disabled');
    }

    return (
        <div className='drop-down-child-size'>
            <div className={smallSizeClasses.join(' ')}>
                <p className='size-icon'>{sizeName}</p>
            </div>
        </div>
    )
}

export default Size