import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import * as Container from './Container'
import HomePageContainer from './Container/HomePageContainer'

// const browserHistory = createBrowserHistory();

export const renderRoutes = () => {
    <BrowserRouter>
        <div className="app">
          <Container.NavBarContainer />
          <Switch>
              <Route path="/" exact component={Container.HomePageContainer} />
              <Route path="/prod-list" component={Container.ProductListContainer} />
          </Switch>
        </div>
    </BrowserRouter>
}