import React from 'react'

import { ProductList } from '../Components'

class ProductListContainer extends React.Component {
    render() {
        return (
            <ProductList />
        )
    }
}

export default ProductListContainer