import React from 'react'
import { connect } from 'react-redux'

import { NavBar } from '../Components'
import { toggleAuthenModal } from '../actions/uiActions'

class NavBarContainer extends React.Component {
    render() {
        return (
            <NavBar {...this.props}/>
        )
    }
}

const mapStateToProps = ({ ui }) => ({ ui });

export default connect(mapStateToProps, { toggleAuthenModal })(NavBarContainer)