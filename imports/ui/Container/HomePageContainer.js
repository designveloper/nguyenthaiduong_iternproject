import React from 'react'

import { HomePage } from '../Components'

class HomePageContainer extends React.Component {
    render() {
        return (
            <HomePage />
        )
    }
}

export default HomePageContainer