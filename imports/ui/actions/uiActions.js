import { TOGGLE_MODAL, TOGGLE_MODAL_AUTHEN } from './types'

export const toggleModal = activateModal => dispatch => {

    return dispatch({
        type: TOGGLE_MODAL,
        payload: activateModal
    })
}

export const toggleAuthenModal = (activateModal, isModalLogin) => dispatch => {

    return dispatch({
        type: TOGGLE_MODAL_AUTHEN,
        payload: {activateModal, isModalLogin}
    })
}