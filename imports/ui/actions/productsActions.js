import { Meteor } from 'meteor/meteor'
import Products from '../../api/products'

import { FETCH_ALL_PRODUCTS, FETCH_A_PRODUCT } from './types'

export const fetchAllProducts = () => dispatch => {
    Meteor.subscribe('fetchAllProducts', function() {
        console.log(Products.find().fetch());
        return dispatch({
            type: FETCH_ALL_PRODUCTS,
            payload: Products.find().fetch()
        })  
    });
}

export const fetchAProduct = product_id => dispatch => {
    Meteor.subscribe('fetchAProduct', product_id, function() {
        console.log(Products.findOne(product_id));

        return dispatch({
            type: FETCH_A_PRODUCT,
            payload: Products.findOne(product_id)
        })
    });
}

export const fetchLimitAmountProducts = (limit, skip = 0) => dispatch => {
    Meteor.subscribe('fetchLimitAmountProducts', limit, skip, function() {
        console.log(Products.find({}, {limit, skip}).fetch());
    })
}