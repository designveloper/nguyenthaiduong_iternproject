import React from 'react';
import { connect } from 'react-redux'
import { BrowserRouter, Route, Switch, withRouter } from 'react-router-dom'

import * as Container from './Container'
import { Footer } from './Components'
import { fetchAllProducts, fetchAProduct, fetchLimitAmountProducts } from '../ui/actions/productsActions'

class App extends React.Component {
  componentDidMount() {
    this.props.fetchAllProducts();
    this.props.fetchAProduct('kHWhjZWMpQvtDWAZm');
    this.props.fetchLimitAmountProducts(2, 1);
  }

  render() {
    console.log(this.props.products);
    return (
      <BrowserRouter>
        <div className="app">
          <Container.NavBarContainer />
          <Switch>
              <Route path="/" exact component={Container.HomePageContainer} />
              <Route path="/prod-list" component={Container.ProductListContainer} />
          </Switch>
          <Footer />
        </div>
      </BrowserRouter>
    )
  }
}

const mapStateToProps = ({products}) => ({products});

export default connect(mapStateToProps, { fetchAllProducts, fetchAProduct, fetchLimitAmountProducts })(App);
