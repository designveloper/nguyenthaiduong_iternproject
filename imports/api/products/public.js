import { Meteor } from 'meteor/meteor'
import Products from '.'

Meteor.publish('fetchData', function() {
    console.log(Products.find().fetch());
    const products = Products.find({});

    return products;
});

Meteor.publish('fetchAllProducts', function() {
    const products = Products.find();

    return products;
})

Meteor.publish('fetchAProduct', function(product_id) {
    const product = Products.find({_id: product_id});

    return product;
})

Meteor.publish('fetchLimitAmountProducts', function(limit, skip = 0) {
    console.log(limit, skip);
    const product = Products.find({}, { skip, limit });

    console.log(Products.find({}, { skip, limit }).fetch());

    return product;
})