import { Meteor } from 'meteor/meteor'
import Products from '.'
import { ProductsSchema } from '.'

Meteor.methods({
    addNewProduct(product) {
        if(schemaCheckErrors(product)) {
            Products.insert(product);
            return { success: true };
        }

        return { success: false };
    },

    removeProduct(product_id) {
        Products.remove(product_id);
    },

    editProduct({product_id, product}) {
        let updatedProduct;

        if(schemaCheckErrors(product)) {
            updatedProduct = Products.update(product_id, { $set: product});
        }

        return { success: true };
    }
});


//validate obj with schema
function schemaCheckErrors(obj) {
    ProductsSchema.validate(obj);

    if(!ProductsSchema.isValid()) {
        ProductsSchema.validationErrors().map(error => {
            throw new Meteor.Error(`Error at ${error.name}, type: ${error.type}, value: ${error.value}`);
        });

        return false;
    }

    return true;
}
