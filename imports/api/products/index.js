import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'
import { check } from 'meteor/check'
import SimpleSchema from 'simpl-schema'

const Products = new Mongo.Collection('products');

export const ProductsSchema = new SimpleSchema({
    name: String,
    size: {
        type: Number,
        min: 0
    },
    color: String,
    brand_id: String,
    price: {
        type: Number,
        min: 0
    },
    available: {
        type: Boolean,
        defaultValue: false
    },
    category_id: String
}, { check }).newContext();

export default Products